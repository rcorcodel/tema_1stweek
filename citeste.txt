Cerinte:
1.	Create a script that will help you to create faster, directories 
2.	Create a script that finds and deteles all the empty files (for this please create in a folder several files – leave some of them empty)
3.	Create a script that will help us know the host name, uptime, ip and who is the current user (hint: uptime, hostname)
4.	Create a script that helps us to update packages in Ubuntu
5.	Create a script that automates several comands: shebang, touch, chmod and vim.

1) Aici am abordat ideea de a creea un director care sa aiba cate un folder pentru fiecare zi din proiect

--- -m seteaza permisii full pentr toti userii care folosesc directorul
--- -p ma lasa sa creez directore parinte si face skip la warning-uri
--- am folosit conceptul de brace expansion ca sa creez folderele mai rapid si dupa patternul urmator

Directory tree output:

Internship/
|-- Martie
    |-- Saptamana1_Luni
    |-- --------------
    |-- Saptamana4_Vineri
|-- Aprilie
    |-- Saptamana5_Luni
    |-- --------------
    |-- Saptamana6_Vineri

Cod:

#!/bin/bash
sudo mkdir -m777 -p Internship/{Martie/Saptamana{1..4}_{Lun,Mar,Mie,Joi,Vin},Aprilie/Saptamana{5..6}_{Lun,Mar,Mie,Joi,Vin}}
echo -e "Directorul a fost creat in folderul curent"

2)Am creat in directorul Internship fisiere goale si in alte doua subdirectoare 

sudo touch Internship/fisier_gol{1..5}
sudo touch Internship/Martie/fisier_gol{1..5}
sudo touch Internship/Martie/fisier_gol{1..5}

--Apoi am creat in directorul Internship/ director 5 fisiere a cate 1 megabyte
sudo truncate -s 1m Internship/fisier_cu_date{1..5}

--- -scriptul va sterge orice fisier gol din directorul dat si subdirectoare inclusiv
--- -print printeaza numele fisierului
--- -delete sterge fisierul

Cod:

#!/bin/bash
sudo find Internship -size 0 -print -delete

3)
--- am folosit | ca sa iau outputul de la comanda ca input pentru xargs (pipe)
--- xargs executa echo by default , dar am specificat totusi comanda ca sa para codul mai lizibil
--- -s extrage numele hostname
--- -p arata time-ul in format --pretty
--- -I arata toate ip-urile host-ului curent

Cod:

#!/bin/bash
hostname -s | xargs echo Numele hostului este:
uptime -p  | xargs echo Uptime:
hostname -I | xargs echo Ip-ul meu este: 
whoami | xargs echo Numele userului curent este:

4)
--- deoarece comenzile trebuie sa fie executate in succesiune le-am combinat intr-una singura prin &&
--- -y da skip la intrebarea daca vrei sa instalezi sau nu update-urile
Cod:

#!/bin/bash
sudo apt update && sudo apt upgrade -y

5)
Am creeat un fisier tip shell si am dat drepturi full la orice utilizator care acceseaza fisierul. O data creat, fisierul se deschide automat in editorul vim o data cu rularea scriptului gata de editare. 

Cod:

#!/bin/bash
touch script_draft.sh
chmod 777 script_draft.sh
echo "#/bin/bash" > script_draft.sh
vim script_draft.sh




